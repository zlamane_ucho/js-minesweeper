function Cell(x, y, w){
	this.x = x;
	this.y = y;
	this.w = w;
	this.count = 0;
	this.bee = false;
	this.revealed = false;
	this.sweep = false;
}

Cell.prototype.show = function(){
	
	//draw grid
	if(!this.sweep){
		canvasContext.rect(this.x, this.y, this.w, this.w);
		canvasContext.strokeStyle = "black"; //grid colour
		canvasContext.lineWidth = 1;
		canvasContext.stroke();

	
	//show revealed cells
	if(this.revealed){
		
		//draw bee if needed
		if(this.bee) {
			canvasContext.beginPath();
			canvasContext.arc(this.x + this.w/2, this.y + this.w/2, this.w/3, 0, Math.PI*2, true);
			canvasContext.fillStyle = "yellow"; //bee colour
			canvasContext.fill();
			canvasContext.strokeStyle = "black";
			canvasContext.stroke();
			canvasContext.closePath();
		}
		else {
			//blank if there is no bee around
			canvasContext.fillStyle = "gray"; //blank colour
			canvasContext.fillRect(this.x, this.y, this.w, this.w);
			
			//show amount of bees near this cell
			if(this.count>=1){
			canvasContext.fillStyle = "black"; //text colour
			canvasContext.font = "40px Times New Roman"; //font size
			canvasContext.fillText(this.count, this.x + this.w/2 - 10, this.y + this.w/2 + 15);
			}
		}
	}
	}
	
	//draw marker for bees
	else
	{
		canvasContext.fillStyle = "red";
		canvasContext.fillRect(this.x			, this.y			, this.w/2	, this.w/2);
		canvasContext.fillStyle = "green";
		canvasContext.fillRect(this.x + this.w/2, this.y			, this.w/2	, this.w/2);
		canvasContext.fillStyle = "yellow";
		canvasContext.fillRect(this.x			, this.y + this.w/2	, this.w/2	, this.w/2);
		canvasContext.fillStyle = "blue";
		canvasContext.fillRect(this.x + this.w/2, this.y + this.w/2	, this.w/2	, this.w/2);
	}
	
}

//getter and setters
{
Cell.prototype.getBee = function(){
	return this.bee;
}

Cell.prototype.setBee = function(isBee){
	this.bee = isBee;
}

Cell.prototype.getCount = function(){
	return this.count;
}

Cell.prototype.getRevealed = function(){
	return this.revealed;
}

Cell.prototype.setSweep = function(){
	this.sweep = !this.sweep;
}

Cell.prototype.getSweep = function(){
	return this.sweep;
} 
}
//checking if cursor is on cell
Cell.prototype.contains = function(x, y){
	return(x > this.x && x < this.x + this.w && y > this.y && y < this.y + this.w)
}

//set reveal
Cell.prototype.reveal = function(){
	
	this.revealed = true;
	if(this.count == 0) this.showEmpty();
	if(this.count == -1) this.endGame();
	
}

//counting how many neighbours of cell are with bee
Cell.prototype.countBees = function(grid){
	var neighbours = 0;
	if(this.bee) neighbours = -1;
	else{
		for(var i = -1; i < 2; i++)
		{
			for(var j = -1; j < 2; j++)
			{
				var n = this.x/this.w;	// position x in grid
				var m = this.y/this.w;	// position y in grid
				if(j==0 && i==0) continue;
				if(n + i < 0 || n + i > grid.length - 1 || m + j < 0 || m + j > grid.length - 1) continue;
				else 
				{
					if(grid[n + i][m + j].getBee() == true) neighbours++;
				}
			}
		}
	}
	this.count = neighbours;
}

//showing all empty cells that are linked
 Cell.prototype.showEmpty = function(){
	var i = this.x / this.w;	// position x in grid
	var j = this.y / this.w;	// position y in grid
	for(var n = -1; n < 2; n++){
		for(var m = -1; m < 2; m++){
			if(m==0 && n==0) continue;
			if(i + n < 0 || i + n > grid.length - 1 || j + m < 0 || j + m > grid.length - 1) continue;
			else {
				var obj = grid[i+n][j+m];
				if(!obj.getRevealed()) obj.reveal();
			}
		}
	}
	
}

//show everything someone clicked on bomb
Cell.prototype.endGame = function(){
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < rows; j++){
			if(!grid[i][j].getRevealed()) grid[i][j].reveal();
		}
	}
}