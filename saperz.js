
var grid;
var cols;
var rows;
var w = 60; //width of cell [600%w should equal 0]  "%" stands for modulo
var beeAmount = 10;
var sweepMines = false;
var coveredMines = 0;
var start = false;
var lostOrEnd = false;

var timer = 0;

var canvas;
var canvasContext;
var sCanvas;
var sCtx;

function calculateMousePos(evt){
	var rect = canvas.getBoundingClientRect();
	var root = document.documentElement;
	var mouseX = evt.clientX - rect.left - root.scrollLeft;
	var mouseY = evt.clientY - rect.top - root.scrollTop;
	return{
		x: mouseX,
		y: mouseY
	};
	
}

window.onload = function(){
	
	canvas = document.getElementById('gameCanvas');
	canvasContext = canvas.getContext('2d');
	sCanvas = document.getElementById('scoreCanvas');
	sCtx = sCanvas.getContext('2d');
	cols = Math.floor(canvas.width/w);
	rows = Math.floor(canvas.height/w);
	grid = make2DArray(cols, rows);
	showTimer();
	initCells();
	makeBees();
	countNeighbour();
	clicker();
	draw();
	
	document.onkeydown = checkKey;
}

function make2DArray(cols, rows) {
	var arr = new Array(cols);
	for(var i = 0; i < arr.length; i++)
	{
		arr[i] = new Array(rows);
	}
	return arr;
}

function initCells(){
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < rows; j++)
		{
			grid[i][j] = new Cell(i*w, j*w, w);
		}
	}
}

function makeBees(){
	var options = [];
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < rows; j++)
		{
			options.push([i,j]);
		}
	}
	for(var n = 0; n < beeAmount; n++){
		var rand = Math.floor(Math.random()*options.length);
		var choice = options[rand];
		var x = choice[0];
		var y = choice[1];
		grid[x][y].setBee(true);	
		options.splice(rand,1);
	}
}

//checking mouse click event
function clicker(){
	canvas.addEventListener('click', function(evt){
		var mousePos = calculateMousePos(evt);
		mousePressed(mousePos.x, mousePos.y);
		//console.log(sweepMines);
		
		if(!start && !lostOrEnd)
		{
			start = true;
			setInterval(showTimer, 100);
		}
		draw();
		if(lostOrEnd){
			canvas.addEventListener('click', function(){location.reload();});
		}
		
	});
}

//draw grid
function draw() {
	canvasContext.fillStyle = "red";
	canvasContext.fillRect(0,0, canvas.width, canvas.height);
	
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < rows; j++)
		{
			grid[i][j].show();
		}
	}
}

function countNeighbour(){
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < rows; j++) grid[i][j].countBees(grid);
	}
}

//action after mouse click event
function mousePressed(x,y){
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < rows; j++){
			if(grid[i][j].contains(x,y)){
				//if sweep mines option is ON and there is less covered than amount
				//or if it is swept cell
				if(sweepMines && coveredMines < beeAmount || grid[i][j].getSweep()) 
				{
					grid[i][j].setSweep();
					if(grid[i][j].getSweep()) coveredMines++;
					else coveredMines--;
					//console.log(coveredMines);
					if(coveredMines == beeAmount && checkWin()) 
					{
						lostOrEnd = true;
						winScreen();
					}
				}
				else if(!sweepMines) 
				{
					grid[i][j].reveal();
					if(grid[i][j].getBee()) 
					{
						start = false;
						lostOrEnd = true;
					}
				}
			}
		}
	}
}

function checkWin(){
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < rows; j++) {
			if(grid[i][j].getSweep())
				if(grid[i][j].getBee()) continue;
				else {
					console.log("ZLE!");
					return false;
				}
		}
	}
	console.log("DOBRZE");
	return true;
}

function winScreen(){
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < rows; j++) {
			if(!grid[i][j].getSweep() && !grid[i][j].getRevealed()){
					grid[i][j].reveal();
				}
		}
	}
}

function checkKey(e) {
	
    e = e || window.event;
	
	//up arrow
    if (e.keyCode == '32') {
		sweepMines = !sweepMines;
	}
  
}

function showTimer(){
	sCtx.fillStyle = "blue";
	sCtx.fillRect(0,0, sCanvas.width, sCanvas.height);
	sCtx.fillStyle = "black"; //<< kolor napisu SCORE
	sCtx.font = "30px Times New Roman";
	timerString = timer + "";
	sCtx.fillText(timerString, canvas.width/2 - timerString.length*7.5, 35);
	if(!lostOrEnd)
	{
		timer+=0.1;
		timer = Math.round(timer*10)/10;
	}
}




	